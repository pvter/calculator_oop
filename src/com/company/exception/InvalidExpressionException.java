package com.company.exception;

public class InvalidExpressionException extends Exception {
    @Override
    public String getMessage() {
        return "Некорректное выражение.";
    }
}
