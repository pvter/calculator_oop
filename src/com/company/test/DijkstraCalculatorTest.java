package com.company.test;

import com.company.calculator.Calculator;
import com.company.calculator.DijkstraCalculator.DijkstraCalculator;
import com.company.exception.EmptyExpressionException;
import com.company.exception.InvalidExpressionException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DijkstraCalculatorTest {
    private final Calculator calculator = new DijkstraCalculator();

    @Test
    public void calculatorTest() throws Exception {
        validExpressionTest("1+1", 2);
        validExpressionTest("-2", -2);
        validExpressionTest("2*2", 4);
        validExpressionTest("2-(-2)", 4);
        validExpressionTest("1+10/5", 3);
        validExpressionTest("((1)+(2))", 3);
    }

    private void validExpressionTest(String expression, double expectedResult) throws Exception {
        assertEquals(expectedResult, calculator.calculate(expression), 0.0000000001);
    }

    @Test
    public void emptyStringTest() {
        Exception exception = assertThrows(RuntimeException.class, () -> calculator.calculate(""));
        String expectedMessage = new EmptyExpressionException().getMessage();
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void invalidExpressionsTest() {
        List<String> expressions = List.of(
                "2-(/2)",
                "2--2",
                "2.(1+2)",
                "2/"
        );
        expressions.forEach(this::invalidExpressionTest);
    }

    private void invalidExpressionTest(String expression) {
        Exception exception = assertThrows(RuntimeException.class, () -> calculator.calculate(expression));
        String expectedMessage = new InvalidExpressionException().getMessage();
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}
