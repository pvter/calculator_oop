package com.company.service.errorHandler;

import com.company.service.output.Output;

public class ErrorHandler {
    private final Output output;

    public ErrorHandler(Output output) {
        this.output = output;
    }

    public void handleError(Exception e) {
        this.output.print("Ошибка: " + e.getMessage());
    }
}
