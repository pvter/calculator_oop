package com.company.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class StringUtil {

    /**
     * Проверка возможности преобразования строки в число
     */
    public static boolean isNumber(String str) {
        if (str == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Форматирование числа
     */
    public static String formatResult(double number) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.UK);
        DecimalFormat format = (DecimalFormat) numberFormat;
        format.setMaximumFractionDigits(10);
        return format.format(number);
    }
}
