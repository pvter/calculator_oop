package com.company.calculator;

import com.company.exception.DivisionException;
import com.company.exception.InvalidOperatorException;

public abstract class Calculator {
    public abstract double calculate(String expression) throws Exception;
    /**
     * Выполнение арифметического действия
     */
    protected double performOperation(char operator, double x, double y) throws Exception {
        switch (operator) {
            case '+':
                return x + y;
            case '-':
                return x - y;
            case '−':
                return -y;
            case '*':
                return x * y;
            case '/':
                if (y == 0) throw new DivisionException();
                return x / y;
        }
        throw new InvalidOperatorException(operator);
    }
}
