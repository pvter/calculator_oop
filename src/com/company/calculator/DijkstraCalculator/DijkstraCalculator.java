package com.company.calculator.DijkstraCalculator;

import com.company.calculator.Calculator;
import com.company.exception.EmptyExpressionException;
import com.company.exception.InvalidExpressionException;
import com.company.validator.CalculatorValidator;
import com.company.validator.DijkstraValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.company.util.CharUtil.isOperator;
import static com.company.util.StringUtil.isNumber;

public class DijkstraCalculator extends Calculator {
    private final CalculatorValidator validator = new DijkstraValidator();

    public double calculate(String expression) throws Exception {
        validator.validate(expression);
        String RPNExpression = RPNConverter.convert(expression);
        return calcResult(RPNExpression);
    }

    private double calcResult(String expression) throws Exception {
        List<String> items = new ArrayList<>(List.of(expression.split(" "))).stream()
                .filter(item -> item.length() > 0).collect(Collectors.toList());

        double result = 0.0;

        if (items.isEmpty()) throw new EmptyExpressionException();
        if (items.size() == 1) {
            if (!isNumber(items.get(0))) throw new InvalidExpressionException();
            return Double.parseDouble(items.get(0));
        }

        while (!items.isEmpty()) {
            OptionalInt optPos = IntStream.range(0, items.size())
                    .filter(i -> items.get(i).length() == 1 && isOperator(items.get(i).charAt(0))).findFirst();
            if (optPos.isEmpty()) break;

            int operatorPosition = optPos.getAsInt();
            if (operatorPosition == 0 || items.get(operatorPosition - 1).isEmpty())
                throw new InvalidExpressionException();

            char o = items.get(operatorPosition).charAt(0);
            double y = Double.parseDouble(items.get(operatorPosition - 1));
            double x = operatorPosition == 1 ? 0 : Double.parseDouble(items.get(operatorPosition - 2));
            result = performOperation(o, x, y);

            if (o == '−') {
                items.set(operatorPosition - 1, Double.toString(result));
                items.remove(operatorPosition);
                continue;
            }

            if (operatorPosition > 1) {
                items.set(operatorPosition - 2, Double.toString(result));
                items.remove(operatorPosition);
                items.remove(operatorPosition - 1);
            } else {
                items.set(0, Double.toString(result));
                items.remove(operatorPosition);
            }
        }
        return result;
    }
}
