package com.company.calculator.DijkstraCalculator;

import com.company.exception.ExtraBracketException;
import com.company.exception.InvalidOperatorException;
import com.company.service.output.ConsoleOutput;
import com.company.service.output.Output;

import java.util.Stack;

import static com.company.util.CharUtil.isDigit;
import static com.company.util.CharUtil.isOperator;

public class RPNConverter {
    private final static Output output = new ConsoleOutput();

    /**
     * Преобразование выражения в формат обратной польской нотации
     */
    public static String convert(String expression) throws Exception {
        StringBuilder output = new StringBuilder();
        Stack<Character> stack = new Stack<>();

        // Пока в исходной строке есть необработанные лексемы, считываем очередную:
        for (int i = 0; i < expression.length(); i++) {
            char currentChar = expression.charAt(i);

            if (isDigit(currentChar) || currentChar == '.') { // Если лексема — число, добавляем в строку вывода.
                output.append(currentChar);
            } else if (currentChar == '(') { // Если лексема — открывающая скобка, помещаем в стек.
                stack.push(currentChar);
            } else if (isOperator(currentChar)) { // Если лексема — Оператор O1.
                int currentCharPriority = getPriority(currentChar);

                // кодируем унарный минус отдельным символом
                if (currentChar == '-' && i > 0 && expression.charAt(i - 1) == '(') currentChar = '−';

                // Пока присутствует на вершине стека лексема-оператор (O2), чей приоритет выше или равен приоритету O1,
                // перекладываем O2 из стека в выходную очередь.
                while (!stack.isEmpty() && isOperator(stack.peek())) {
                    char lastStackItem = stack.peek();
                    int lastStackItemPriority = getPriority(lastStackItem);
                    if (lastStackItemPriority >= currentCharPriority) {
                        output.append(' ');
                        output.append(stack.pop());
                    } else break;
                }

                // Помещаем O1 в стек.
                stack.push(currentChar);

            } else if (currentChar == ')') {  // Если лексема — закрывающая скобка
                // Пока лексема на вершине стека не станет открывающей скобкой, перекладываем лексемы-операторы из стека в выходную очередь.
                while (!stack.isEmpty() && stack.peek() != '(') {
                    output.append(' ');
                    output.append(stack.pop());
                }
                if (!stack.isEmpty() && stack.peek() == '(') stack.pop(); // Удаляем из стека открывающую скобку.
                else
                    throw new ExtraBracketException(); // Если стек закончился до того, встретилась открывающая скобка — в выражении содержится ошибка.
            }

            if (isOperator(currentChar)) output.append(' ');
        }

        // Если во входной строке больше не осталось лексем, пока есть операторы в стеке -
        // перекладываем оператор из стека в выходную очередь.
        while (!stack.isEmpty()) {
            char lastItem = stack.pop();
            // Если на вершине стека скобка — в выражении допущена ошибка.
            if (lastItem == '(' || lastItem == ')') throw new ExtraBracketException();
            output.append(' ');
            output.append(lastItem);
        }

        String result = output.toString().trim();
        printOutput(result);
        return result;
    }

    /**
     * Получение приоритета оператора
     */
    private static int getPriority(char c) throws Exception {
        switch (c) {
            case '*':
            case '/':
                return 1;
            case '+':
            case '-':
            case '−':
                return 0;
        }
        throw new InvalidOperatorException(c);
    }

    /**
     * Вывод выражения в консоль
     */
    private static void printOutput(String outputExpression) {
        output.print("Выражение в обратной польской нотации:");
        output.print(outputExpression);
    }
}
