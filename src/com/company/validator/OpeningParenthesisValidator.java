package com.company.validator;

import com.company.exception.InvalidExpressionException;

import static com.company.util.CharUtil.isDigit;
import static com.company.util.CharUtil.isOperator;

public class OpeningParenthesisValidator implements CharValidator {
    public void validate(Character currentChar, Character prevChar, Character nextChar) throws Exception {
        if (currentChar != '(') return;
        if (prevChar != null && !isOperator(prevChar) && prevChar != '(')
            throw new InvalidExpressionException();
        if (nextChar != null && !isDigit(nextChar) && nextChar != '(' && nextChar != '-' && nextChar != '+')
            throw new InvalidExpressionException();
    }
}
