package com.company.validator;

import com.company.exception.EmptyExpressionException;

public class EmptyStringValidator implements StringValidator {
    public void validate(String str) throws Exception {
        if (str.isEmpty()) throw new EmptyExpressionException();
    }
}
