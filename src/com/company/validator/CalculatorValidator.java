package com.company.validator;

public interface CalculatorValidator {
    void validate(String str) throws Exception;
}
