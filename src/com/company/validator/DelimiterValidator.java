package com.company.validator;

import com.company.exception.InvalidExpressionException;

import static com.company.util.CharUtil.isDigit;

public class DelimiterValidator implements CharValidator {
    public void validate(Character currentChar, Character prevChar, Character nextChar) throws Exception {
        if (currentChar != '.') return;
        if (prevChar == null || !isDigit(prevChar)) throw new InvalidExpressionException();
        if (nextChar == null || !isDigit(nextChar)) throw new InvalidExpressionException();
    }
}
