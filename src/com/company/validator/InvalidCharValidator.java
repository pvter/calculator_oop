package com.company.validator;

import java.util.List;
import java.util.stream.Stream;

import static com.company.util.CharUtil.isDigit;
import static com.company.util.CharUtil.isOperator;

public class InvalidCharValidator implements StringValidator {
    List<Character> validChars;

    public InvalidCharValidator(List<Character> validChars) {
        this.validChars = validChars;
    }

    public void validate(String str) throws Exception {
        Stream.of(str.split("")).forEach(ch -> {
            try {
                validateChar(ch.charAt(0));
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        });
    }

    private void validateChar(char ch) throws Exception {
        if (!isDigit(ch) && !isOperator(ch) && !isValidChar(ch)) {
            String message = getInvalidCharMessage(ch);
            throw new Exception(message);
        }
    }

    private boolean isValidChar(char ch) {
        return validChars.contains(ch);
    }

    private String getInvalidCharMessage(Character ch) {
        String message = "недопустимый символ: ";
        if (ch == ' ') message += "Пробел";
        else message += ch;
        message += "\nВыражение может содержать только числа, операторы +,-,*,/, скобки и десятичный разделитель(.)";
        return message;
    }
}
