package com.company.validator;

import com.company.exception.InvalidExpressionException;

import static com.company.util.CharUtil.isOperator;

public class OperatorValidator implements CharValidator {
    public void validate(Character currentChar, Character prevChar, Character nextChar) throws Exception {
        if (!isOperator(currentChar)) return;
        if(nextChar == null) throw new InvalidExpressionException();
        if (prevChar != null && isOperator(prevChar)) throw new InvalidExpressionException();
        if (isOperator(nextChar)) throw new InvalidExpressionException();
    }
}
